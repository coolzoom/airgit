# 
# Translators:
# Rui <xymarior@yandex.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: AirDC++\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-24 21:02\n"
"PO-Revision-Date: 2019-02-11 23:49+0000\n"
"Last-Translator: Rui <xymarior@yandex.com>\n"
"Language-Team: Portuguese (Portugal) (http://www.transifex.com/airdcpp/airdcpp/language/pt_PT/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_PT\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. RunAtFinish
#: AirDC_installscript.nsi:92
msgid "Launch AirDC++"
msgstr "Iniciar AirDC++"

#. ^ComponentsText
#: AirDC_installscript.nsi:93
msgid "Welcome to the AirDC++ installer."
msgstr "Bem-vindo ao instalador do AirDC++."

#. ^DirText
#: AirDC_installscript.nsi:94
msgid ""
"If you are upgrading from an older version, select the same directory where "
"your old version resides and your existing settings will be imported."
msgstr "Se está a atualizar de uma versão antiga, selecione a mesma pasta onde está a versão antiga para que as suas configurações existentes sejam importadas."

#. SecAirDC
#: AirDC_installscript.nsi:95
msgid "AirDC++ (required)"
msgstr "AirDC++ (necessário)"

#. ProfileDir
#: AirDC_installscript.nsi:96
msgid ""
"Settings of a previous installation of AirDC++ has been found in the user "
"profile, do you want to backup settings? (You can find them in $DOCBACKUPDIR"
" later)"
msgstr "Foi encontrada uma versão anterior do AirDC++ e as respetivas configurações no perfil do utilizador. Quer fazer uma cópia de segurança dessas configurações? (Pode encontrá-las posteriormente em $DOCBACKUPDIR)"

#. ProgramDir
#: AirDC_installscript.nsi:97
msgid ""
"A previous installation of AirDC++ has been found in the target folder, do "
"you want to backup settings? (You can find them in $INSTBACKUPDIR later)"
msgstr "Foi encontrada uma versão anterior do AirDC++ na pasta de destino. Quer fazer uma cópia de segurança das configurações? (Pode encontrá-las posteriormente em $INSTBACKUPDIR)"

#. Dcpp
#: AirDC_installscript.nsi:98
msgid ""
"Settings of an existing DC++ installation has been found in the user "
"profile, do you want to import settings and queue?"
msgstr "Foi encontrada uma versão anterior do DC++ e as respetivas configurações no perfil do utilizador. Quer importar as configurações e a fila de transferências?"

#. Apex
#: AirDC_installscript.nsi:99
msgid ""
"Settings of an existing ApexDC++ installation has been found in the user "
"profile, do you want to import settings and queue?"
msgstr "Foi encontrada uma versão anterior do ApexDC++ e as respetivas configurações no perfil do utilizador. Quer importar as configurações e a fila de transferências?"

#. Strong
#: AirDC_installscript.nsi:100
msgid ""
"Settings of an existing StrongDC++ installation has been found in the user "
"profile, do you want to import settings and queue?"
msgstr "Foi encontrada uma versão anterior do StrongDC++ e as respetivas configurações no perfil do utilizador. Quer importar as configurações e a fila de transferências?"

#. SecStartMenu
#: AirDC_installscript.nsi:101
msgid "Start menu shortcuts"
msgstr "Atalhos no menu iniciar"

#. SM_AIRDCPP_DESC
#: AirDC_installscript.nsi:102
msgid "AirDC++ File Sharing Application"
msgstr "AirDC++ Aplicação de Partilha de Ficheiros"

#. SM_AIRDCPP_DESCUN
#: AirDC_installscript.nsi:103
msgid "Uninstall AirDC++"
msgstr "Desinstalar o AirDC++"

#. SM_UNINSTALL
#: AirDC_installscript.nsi:104
msgid "Uninstall"
msgstr "Desinstalar"

#. WR_REMOVEONLY
#: AirDC_installscript.nsi:105
msgid "(remove only)"
msgstr "(apenas remover)"

#. SecEmopack
#: AirDC_installscript.nsi:106
msgid "Default emoticon pack"
msgstr "Pacote de emoticons padrão"

#. SecThemes
#: AirDC_installscript.nsi:107
msgid "Themes"
msgstr "Temas"

#. SecWebResources
#: AirDC_installscript.nsi:108
msgid "Web user interface"
msgstr "Interface de utilizador de web"

#. SecStore
#: AirDC_installscript.nsi:109
msgid "Store settings in the user profile directory"
msgstr "Guardar configurações na pasta do perfil do utilizador"

#. DeskShort
#: AirDC_installscript.nsi:110
msgid "Desktop shortcut"
msgstr "Atalho na área de trabalho"

#. SecInstall32
#: AirDC_installscript.nsi:111
msgid "Install the 32-bit version"
msgstr "Instalar a versão 32-bit"

#. XPOrOlder
#: AirDC_installscript.nsi:112
msgid ""
"If you want to keep your settings in the program directory, make sure that "
"you DO NOT install AirDC++ to the 'Program files' folder!!! This can lead to"
" abnormal behaviour like loss of settings or downloads!"
msgstr "Se quiser manter as suas configurações da pasta do programa, certifique-se que não instala o AirDC++ na pasta 'Ficheiros de programas' ou 'Program files'! Isto pode levar a um comportamento anormal como a perda de configurações ou ficheiros descarregados!"

#. OsUnsupported
#: AirDC_installscript.nsi:113
msgid ""
"This application is compatible with Windows 7 SP1 or newer operating systems"
" only. The installer will now exit."
msgstr "As aplicação é compatível apenas com o sistema operativo Windows 7 SP1 ou mais recente. O instalador irá ser fechado agora."

#. ^UninstallText
#: AirDC_installscript.nsi:114
msgid "This will uninstall AirDC++. Hit the Uninstall button to continue."
msgstr "Isto irá desinstalar o AirDC++. Use o botão Desinstalar para continuar."

#. RemoveQueue
#: AirDC_installscript.nsi:115
msgid "Do you also want to remove queue, themes and all settings?"
msgstr "Também quer remover a fila de descarregamentos, temas e todas as configurações?"

#. NotEmpty
#: AirDC_installscript.nsi:116
msgid "Installation directory is NOT empty. Do you still want to remove it?"
msgstr "A pasta de instalação NÃO está vazia. Ainda quer removê-la?"

#. DESC_dcpp
#: AirDC_installscript.nsi:117
msgid "AirDC++ main program."
msgstr "Programa principal do AirDC++."

#. DESC_StartMenu
#: AirDC_installscript.nsi:118
msgid "A shortcut of AirDC++ will be placed in your start menu."
msgstr "Será colocado no menu Iniciar um atalho para o AirDC++."

#. DESC_Emopack
#: AirDC_installscript.nsi:119
msgid "Display emoticons as images in chat windows."
msgstr "Mostrar emoticons como imagens em janelas de conversação."

#. DESC_Themes
#: AirDC_installscript.nsi:120
msgid ""
"If you don't like the default theme you can try these two extra themes, Dark"
" Skull and Zoolution."
msgstr "Se não gosta do tema padrão, pode experimentar estes dois temas extra, Dark Skull e Zoolution."

#. DESC_WebResorces
#: AirDC_installscript.nsi:121
msgid "Allows accessing the client from other devices via a web browser."
msgstr "Permite aceder ao cliente a partir de outros dispositivos através de um navegador de Internet."

#. DESC_loc
#: AirDC_installscript.nsi:122
msgid ""
"Normally you should not change this because it can lead to abnormal "
"behaviour like loss of settings or downloads. If you unselect it, read the "
"warning message carefully."
msgstr "Normalmente não deve alterar isto porque pode levar a um comportamento anormal como a perda de configurações e ficheiros transferidos Se desselecionar, leia o aviso com cuidado."

#. DESC_desk
#: AirDC_installscript.nsi:123
msgid "Do you often run AirDC++? Then this is definitely something for you!"
msgstr "Costuma abrir muitas vezes o AirDC++? Então isto é definitivamente para si!"

#. DESC_arch
#: AirDC_installscript.nsi:124
msgid ""
"If you want to use the 32-bits version of AirDC++ on your 64-bits operating "
"system you need to select this. If this is selected and grayed out you can "
"not unselect it because your operation system is 32-bits."
msgstr "Se quiser usar a versão 32-bits do AirDC++ num sistema operativo de 64-bits tem de selecionar isto. Se isto for selecionado e estiver a cinzento, quer dizer que o seu sistema operativo é de 32-bits."
